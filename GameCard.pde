class GameCard {

  boolean clicked = false;

  public int cardNumber = -1;

  public int shiftY = 40;
  public int shiftX = 250;

  public float cardH = ((height- shiftY) * 3) / (4.0 * cardVertical - 1); 
  public float cardW = ((width- shiftX) * 3) / (4.0 * cardHorizon - 1);

  public boolean matched = false;

  int positionX, positionY;

  GameCard(int positionX, int positionY) {
    this.positionX = positionX;
    this.positionY = positionY;
    drawCard();
  }

  public void drawCard() {
    if (clicked && !matched) {
      strokeWeight(5);
      stroke(255, 0, 0);
      fill(255);
    } else if (matched) {
      strokeWeight(5);
      stroke(0);
      fill(0);
    } else {
      strokeWeight(5);
      stroke(255);
      fill(255);
    }

    rect(shiftX/2 + (1.33* positionX * cardW), shiftY/2 + (1.33 * positionY * cardH), cardW, cardH);
    if (clicked && !matched) {
      fill(255, 255, 0);
      textFont(gameFont, 48);
      text(cardNumber, shiftX/2 + (1.33* positionX * cardW)+cardW/4, shiftY/2 + (1.33 * positionY * cardH)+cardH*2/3);
    }
  }

  public void updateClick() {
    this.clicked = !clicked;
    drawCard();
  }

  public boolean isClicked() {
    return clicked;
  }

  public void makeMatched() {
    this.matched = true;
    cardLeftUnMatched--;
    drawCard();
  }
}