// Author : Zafer Çavdar
// Processing

GameUser user;
PFont gameFont;

boolean cardsDivided = false;
boolean game_started = false;
boolean game_finished = false;
boolean boardOnTheScreen = false;

public float cardHorizon = 5;
public float cardVertical = 4;
int cardLeftUnMatched = int(cardHorizon*cardVertical);

int startTime = 0;
int stopTime = 0;

int selectedCardNumber = 0;

int sCardX;
int sCardY;
int sCard2X;
int sCard2Y;

GameCard[][] gameCard = new GameCard[(int)cardHorizon][(int)cardVertical];

void setup() {
  size (800, 450);
  background(0);

  gameFont = loadFont("gameFont.vlw");

  getUserData();
  postSMS();
  checkPostedCode();
}


void draw() {

  if (!boardOnTheScreen && game_started && !game_finished) {
    drawBoard();
  }

  if (!cardsDivided) {
    divideCards();
    cardsDivided = true;
    startTime = millis();
  }

  if (cardLeftUnMatched == 0 && !game_finished) {
    game_finished = true;
    stopTime = millis();
    scorePage();
  }
}

void scorePage() {
  background(255, 255, 0);
  textFont(gameFont, 48);
  fill(0);
  int score = stopTime-startTime;
  text("SCORE: " + score, width/4, height/2+20);
  user.setScore(score);
}

void drawBoard() {
  for (int i = 0; i < cardHorizon; i++)
    for (int j = 0; j < cardVertical; j++)
      gameCard[i][j] = new GameCard(i, j);

  boardOnTheScreen = true;
}

void divideCards() {

  int[] numberFrequency = new int[(int)(cardHorizon*cardVertical)];

  for (int i = 0; i < cardHorizon; i++)
    for (int j = 0; j < cardVertical; j++) {
      int num = (int)random(0, 10);

      while (numberFrequency[num] == 2) {
        num = (int)random(0, 10);
      }

      numberFrequency[num]++;
      gameCard[i][j].cardNumber = num;
    }
}

void mousePressed() {

  if (game_started) {
    int rX = regionX(mouseX);
    int rY = regionY(mouseY);

    if (validIndex(rX, rY)) {
      if (selectedCardNumber == 0) {
        sCardX = rX;
        sCardY = rY;
        gameCard[sCardX][sCardY].updateClick();
        selectedCardNumber++;
      } else if (selectedCardNumber == 1) {
        if (!(rX == sCardX && rY == sCardY)) {
          sCard2X = rX;
          sCard2Y = rY;
          gameCard[sCard2X][sCard2Y].updateClick();

          selectedCardNumber++;
          if (gameCard[sCardX][sCardY].cardNumber == gameCard[sCard2X][sCard2Y].cardNumber) {
            gameCard[sCardX][sCardY].makeMatched();
            gameCard[sCard2X][sCard2Y].makeMatched();
          }
        }
      } else {
        gameCard[sCardX][sCardY].updateClick();
        gameCard[sCard2X][sCard2Y].updateClick();

        sCardX = rX;
        sCardY = rY;
        gameCard[sCardX][sCardY].updateClick();
        selectedCardNumber = 1;
      }
    }
  }
}

int regionX(double x) {
  int region = -1;

  for (int i = 0; i < cardHorizon; i++) {
    if (x >= gameCard[0][0].shiftX/2 + (1.33* i * gameCard[0][0].cardW) && x<= gameCard[0][0].shiftX/2 + (1.33* i * gameCard[0][0].cardW) + gameCard[0][0].cardW )
      region = i;
  }

  return region;
}

int regionY(double y) {
  int region = -1;

  for (int i = 0; i < cardVertical; i++) {
    if (y >= gameCard[0][0].shiftY/2 + (1.33* i * gameCard[0][0].cardH) && y<= gameCard[0][0].shiftY/2 + (1.33* i * gameCard[0][0].cardH) + gameCard[0][0].cardH )
      region = i;
  }
  return region;
}

boolean validIndex(int x, int y) {
  return x != -1 && y != -1 && !gameCard[x][y].matched;
}