class GameUser {

  public String phoneNumber;
  int gameScore;
  String generatedKey;

  GameUser(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public void setScore(int score) {
    gameScore = score;
  }

  public int getScore() {
    return gameScore;
  }

  public void setKey(String keyy) {
    generatedKey = keyy;
  }

  public String getKey() {
    return generatedKey;
  }
}

